<?php
wp_enqueue_script('wpscript');
if (isset($_POST["id"])) {
    global $wpdb;
    $table_name=$wpdb->prefix . "enrollment";
    $idt=$_POST["id"];
    if(isset($_POST["action"])) {
        foreach ($idt as $id) {
            $id=  intval($id);
            $temp=array("id" => $id);
            $wpdb->delete($table_name, $temp);
        }
        print "<div class='updated'><p>";
        _e('Selected enrollments deleted!',PLUGIN_NAME);
        print "</p></div>";
    }
}
?>
<div class="wrap">
    <h2><?php _e('Enrollments',PLUGIN_NAME);?></h2>
    <form method="post" action="">
        <div class="tablenav top">
            <div class="alignleft action">
                <select name="action">
                    <option value="2"><?php _e('Delete',PLUGIN_NAME);?></option>
                </select>
                <input type="submit" value="<?php _e('Accept',PLUGIN_NAME); ?>" class="button action">
            </div>
        </div>
    <table class="wp-list-table widefat">
        <thead>
            <tr>
                <th class="mange-column check-column"></th>
                <th class="column-title"><?php _e('Last name',PLUGIN_NAME); ?></th>
                <th><?php _e('First name',PLUGIN_NAME); ?></th>
                <th><?php _e('Email',PLUGIN_NAME); ?></th>
                <th><?php _e('Additional information',PLUGIN_NAME); ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr class="">
                <th></th>
                <th><?php _e('Last name',PLUGIN_NAME); ?></th>
                <th><?php _e('First name',PLUGIN_NAME); ?></th>
                <th><?php _e('Email',PLUGIN_NAME); ?></th>
                <th><?php _e('Additional information',PLUGIN_NAME); ?></th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            global $wpdb;
            $table_name=$wpdb->prefix . "enrollment";
            $sql="SELECT * FROM " . $table_name . " ORDER BY lastname";
            $enrollmetns=$wpdb->get_results($sql);
            
            if($enrollmetns) {
                foreach ( $enrollmetns as $enrollment) {
                    print "<tr>";
                    print "<td><input type='checkbox' name='id[]' value='" . $enrollment->id . "'></td>";
                    print "<td>" . sanitize_text_field($enrollment->lastname);
                    print "<div class='row-actions'>";
                    print "<span class='view'><a href=''>" . __('Send email',PLUGIN_NAME) . "</a></span>";
                    print " | ";
                    print "<span class='trash'><a href='#' id='$enrollment->id' class='submitdelete'>" . __('Delete',PLUGIN_NAME) . "</a></span>";
                    print "</div>";
                    print "</td>";
                    print "<td>" . sanitize_text_field($enrollment->firstname) . "</td>";
                    print "<td>" . sanitize_text_field($enrollment->email) . "</td>";
                    print "<td>" . sanitize_text_field($enrollment->additional_information) . "</td>";
                    print "</tr>";
                }
            }
            ?>        
        </tbody>
    </table>
        <div class="tablenav bottom">
            <div class="alignleft action">
                <select name="action">
                    <option value="2"><?php _e('Delete',PLUGIN_NAME);?></option>
                </select>
                <input type="submit" value="<?php _e('Accept', PLUGIN_NAME);?>" class="button action">
            </div>
        </div>
    </form>
</div>