<?php
wp_enqueue_style('wpstyle');
/* KaKo lisäykset / siirrot */
global $wpdb;
$table_name=$wpdb->prefix . "event";
/* KaKo lisäykset / siirrot */
if (isset($_POST["event"])) {
?>
<div class="updated">
    <p>
    <?php
    $name = sanitize_text_field($_POST["event"]);
    $time = $_POST["time"];
    $description = sanitize_text_field($_POST["description"]);
    $event = $wpdb->get_row("SELECT * FROM ". $table_name);
    if($event==null) {
        $wpdb->insert (
            $table_name,
            array(
                    'name' => $name,
                    'time' => $time,
                    'description' => $description,
                    'active' => true
                )
            );
    }
    else {
        $id=$event->id;
        $wpdb->update (
                $table_name,
                array(
                    'name' => $name,
                    'time' => $time,
                    'description' => $description,
                    'active' => true
                ),
                array('id'=>$id)
            );
    }
    _e('Settings saved.',PLUGIN_NAME);
?>
    </p>
</div>
<?php
}
else {
    $event=$wpdb->get_row("SELECT * FROM " . $table_name);
    if($event!=null) {
        $name=$event->name;
        $time=$event->time;
        $description=$event->description;
        }
    else {
        $name="";
        $time="";
        $description="";
        }
}
?>
<div class="wrap">
    <h2><?php _e('Event Settings', PLUGIN_NAME); ?></h2>
    <form method="post" action="">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">
                        <label for="event"><?php _e('Event',PLUGIN_NAME); ?>:</label>
                    </th>
                    <td>
                        <input id="event" name='event' size="30" maxlength="30" value="<?php print($name);?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="time"><?php _e('Time',PLUGIN_NAME); ?>:</label>
                    </th>
                    <td>
                        <input id="time" name='time' size="30" maxlength="50" value="<?php print($time);?>"
                               placeholder="0000-00-00 00:00:00">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="description"><?php _e('Description',PLUGIN_NAME); ?>:</label>
                    </th>
                    <td>
                        <textarea id="description" name='description'> <?php print($description);?></textarea>
                    </td>
                </tr>                
            </tbody>
        </table>
        <input type='submit' class='button button-primary' value='<?php _e('Save', PLUGIN_NAME) ?>'>
    </form>
</div>