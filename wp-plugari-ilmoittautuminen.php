<?php
wp_enqueue_style('wpstyle');

global $wpdb;

$description="";
$time="";
$fname="";
$lname="";
$email="";
$additional_information="";
$disabled="";

$table_name=$wpdb->prefix . "event";
$event=$wpdb->get_row("SELECT * FROM " . $table_name);

if($event!=null) {
    $description=$event->description;
    $time=$event->time;
}
?>
<?php
if (isset($_POST["fname"])) {
?>
<div class='updated'>
    <p>
        <?php
        $fname = sanitize_text_field($_POST["fname"]);
        $lname = sanitize_text_field($_POST["lname"]);
        $email = sanitize_text_field($_POST["email"]);
        $additional_information = strip_tags($_POST["additional_information"]);
        
        if($event!=null) {
            $event_id=$event->id;
            $table_name=$wpdb->prefix . "enrollment";

            $wpdb->show_errors = false;
            
            $result=$wpdb->insert(
                    $table_name,
                    array(
                        'firstname' => $fname,
                        'lastname' => $lname,
                        'email' => $email,
                        'additional_information' => $additional_information,
                        $wpdb->prefix . 'event_id' => $event_id
                        )
                    );
            
            if($result > 0 ) {
                _e('Thank you for your enrollment.', PLUGIN_NAME);
                $disabled="disabled";                
                }
            else {
                _e('Your enrollment could not be saved.', PLUGIN_NAME);
                $disabled="";
            }

        }
        else {
            _e('There is no event that you could enroll!.',PLUGIN_NAME);
        }
        ?>    
    </p>
</div>
<?php
}
else {
    $user=wp_get_current_user();
    if($user->ID!=0 ) {
        $fname=$user->user_firstname;
        $lname=$user->user_lastname;
        $email=$user->user_email;
    }
}
?>
<div class="entry_content">
    <h3>
            <?php print $time;?>
            <br />
            <?php print $description;?>
    </h3>
    <form method="post" action="">
        <label for=""><?php _e('First name',PLUGIN_NAME); ?>:</label>
        <input id="fname" name="fname" size="30" maxlength="50" value="<?php echo $fname;?>">
        <label for=""><?php _e('Last name',PLUGIN_NAME); ?>:</label>
        <input id="lname" name="lname" size="30" maxlength="50" value="<?php echo $lname;?>">
        <label for=""><?php _e('Email address',PLUGIN_NAME); ?>:</label>
        <input id="email" name="email" size="30" maxlength="100" value="<?php echo $email;?>">
        <label for=""><?php _e('Additional information',PLUGIN_NAME); ?>:</label>
        <textarea id="additional_information" name="additional_information">
            <?php echo $additional_information; ?></textarea>
        <div class="buttons">
            <input type="submit" value="<?php _e('Send',PLUGIN_NAME);?>"
                <?php print($disabled);?>>
            <input type="reset" value="<?php _e('Reset', PLUGIN_NAME);?>">
        </div>
    </form>
</div>