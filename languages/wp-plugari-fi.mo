��          �      |      �     �               3     9  
   G     R     ^     d  
   s  	   ~     �     �     �     �  
   �     �  3   �     �  '        D  �  I     �     �  &   �               *     ;  	   K     U     i     q     z  	   �     �     �     �     �  7   �  '     7   5     m                            
   	                                                                  Additional information Description Displays number of enrollments Email Email address Enrollment Enrollments Event Event Settings First name Last name Number of enrollments Reset Save Send Send Email Settings saved. Subject and message will be asked before submission Thank you for your enrollment. There is no event that you can enroll!. Time Project-Id-Version: JJ Example Plugin
POT-Creation-Date: 2014-04-18 12:39+0200
PO-Revision-Date: 2014-04-18 12:39+0200
Last-Translator: 
Language-Team: 
Language: Finnish
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: __;_e;_
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: admin
 Lisätietoja Kuvaus Näyttää ilmoittautuneiden määrän Sähköposti Sähköposti Ilmoittautuminen Ilmoittautuneet Tapahtuma Tapahtuma-asetukset Etunimi Sukunimi Ilmoittautumisia Tyhjennä Tallenna Lähetä Lähetä sähköpostia Asetukset tallennettu. Viestin aihe ja sisältö kysytään ennen lähetystä. Ilmoittautuminen vastaanotettu. Kiitos. Tapahtumia ei ole. Ilmoittautuminen ei ole mahdollista. Aika 