<?php
class Wp_plugari_vimpain extends WP_Widget {
    public function __construct() {
        parent::__construct(
                'wp_plugari_vimpain',
                __('Enrollment',PLUGIN_NAME),
                array('desription' =>__('Display number of enrollments',PLUGIN_NAME),)
                );
    }
    
    public function widget($args, $instance) {
        global $wpdb;
        
        $enrolment_count=$wpdb->get_var("SELECT COUNT(id) FROM " . $wpdb->prefix . "enrollment");
        
        print "<aside class='widget'>";
        print "<p>" . __('Number of enrollments',PLUGIN_NAME) . "<br />" . $enrolment_count . "</p>";
        print "</aside>";
    }
    
    public function form($instance) {
        // No Widget setting for administrator
    }
    
    public function update($new_instance,$old_instance) {
        // Widget does not have nothing to save
    }
}
