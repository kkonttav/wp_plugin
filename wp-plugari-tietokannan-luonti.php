<?php
class Wp_plugari_tietokannan_luonti {

public static function activate() {
    global $wpdb;
    $table_name1=$wpdb->prefix . "event";
    
    $sql = "CREATE TABLE IF NOT EXISTS $table_name1 (
            id int PRIMARY KEY AUTO_INCREMENT,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            name varchar(50) NOT NULL,
            description text,
            active bool
            );";
            
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");
    dbDelta($sql);

    $table_name2=$wpdb->prefix . "enrollment";
    $sql = "CREATE TABLE IF NOT EXISTS " . $table_name2 . "( ";
    $sql .= "id int PRIMARY KEY AUTO_INCREMENT,";
    $sql .= "time timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,";
    $sql .= "firstname varchar(50) NOT NULL,";
    $sql .= "lastname varchar(50)NOT NULL,";
    $sql .= "email varchar(100) NOT NULL UNIQUE,";
    $sql .= "additional_information text,";
    $sql .= $table_name1 . "_id int NOT NULL,";
    $sql .= "INDEX par_ind (" . $table_name1 . "_id),";
    $sql .= "FOREIGN KEY (" . $table_name1. "_id) REFERENCES " . $table_name1 . "(id) ";
    $sql .= "ON DELETE RESTRICT);";
    
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");
    dbDelta($sql);
    
    add_option("wp-plugari-db-version","0.1");
    }

public static function uninstall() {
    global $wpdb;
    $table_name  =$wpdb->prefix . "enrollment";
    $sql = "DROP TABLE IF EXISTS $table_name;";
    $wpdb->query($sql);
    $table_name = $wpdb->prefix . "event";
    $sql = "DROP TABLE IF EXISTS $table_name;";
    $wpdb->query($sql);
    delete_option("wp-plugari-db-version");
    }

}