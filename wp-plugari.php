<?php
/*
Plugin Name: wp-plugari
Plugin URl: http://foo.com
Description: Esimerkki WP-lisäosan toteuttamisesta, Toteutettu olio-ohjelmointia käyttäen.
Version: 0.1
Author: Kari Konttavaara
License: GPL2
*/
?>
<?php
define('PLUGIN_NAME','wp-plugari');

include_once('wp-plugari-vimpain.php');
include_once('wp-plugari-class.php');
include_once('wp-plugari-tietokannan-luonti.php');

//Add hooks
register_activation_hook(__FILE__,array('Wp_plugari_tietokannan_luonti', 'activate'));
register_uninstall_hook(__FILE__,array('Wp_plugari_tietokannan_luonti', 'uninstall'));
