<?php
class Wp_plugari_class {

    public function __construct() {
        load_plugin_textdomain(PLUGIN_NAME, false, basename(dirname( __FILE__ ) ). '/languages');
        
        add_action('init', array($this, 'init'));
        add_action('widgets_init',array($this,'register_widget'));
        add_action('admin_menu', array($this, 'setup_admin_menu'));
        
        add_filter('the_title', array($this,'change_enrollment_form_post_title'),10,2);
        add_filter('wp_title', array($this,'change_enrollment_form_wp_title'),10,2);
    }
    
    public function init() {
        wp_register_style('wpstyle', plugins_url('css/style.css', __FILE__));
        wp_register_script('wpscript',plugins_url('js/wp_enroll.js',__FILE__));
        add_shortcode('Enroll',array($this,'enroll_shortcode'));
    }
    
    public function setup_admin_menu() {
        add_object_page(PLUGIN_NAME,__('Enrollment',PLUGIN_NAME),'manage_options', PLUGIN_NAME, array($this, 'admin_page'));
        add_submenu_page(PLUGIN_NAME,__('Event Settings', PLUGIN_NAME),__('Event Settings',PLUGIN_NAME),'manage_options',PLUGIN_NAME,array($this,'admin_page'));
        add_submenu_page(PLUGIN_NAME,__('Enrollments',PLUGIN_NAME),__('Enrollments',PLUGIN_NAME),'manage_options','submenu',array($this,'admin_enrollments'));
    }
    
    public function register_widget() {
        register_widget('Wp_plugari_vimpain');
    }

    public function enroll_shortcode(){
        include_once(plugin_dir_path(__FILE__) . 'wp-plugari-ilmoittautuminen.php');
    }

    public function admin_enrollments() {
        include_once(plugin_dir_path(__FILE__) . 'admin/wp-enrollments.php');
    }

        public function admin_page() {
        include_once(plugin_dir_path(__FILE__) . 'admin/wp-settings.php');
    }
    
    public function change_enrollment_form_post_title($title,$id) {
        if($title=='wp-plugari-ilmoittautuminen') {
            global $wpdb;
            $table_name=$wpdb->prefix . "event";
            
            $event=$wpdb->get_row("SELECT * FROM " . $table_name);
            return $event->name;
        }
        else {
            return $title;
        }
    }
    
    public function change_enrollment_form_wp_title($title,$id) {
            if(substr($title,0,24)=='wp-plugari-ilmoittautuminen') {
            global $wpdb;
            
            $table_name=$wpdb->prefix . "event";
            
            $event=$wpdb->get_row("SELECT * FROM " . $table_name);
            return $event->name . " | ";
        }
        else {
            return $title;
        }
    }
    
}

add_action('plugins_loaded','wp_plugari_init');

function wp_plugari_init() {
    $wp_plugari = new Wp_plugari_class();
}